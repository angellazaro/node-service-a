// define our app using express
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8002;        // set our port

// ROUTES FOR OUR API
// =============================================================================
// get an instance of the express Router
var router = express.Router();

// test route to make sure everything is working (accessed at GET http://localhost:8001/api)
router.get('/', function(req, res) {
	console.log('node-service-a: /');
});

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------

// Contenedores
router.route('/service-a-1')
	// List all containers
	.get(function(req, res) {
		console.log('node-service-a: /service-a-1');
	})
;


// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);